package com.anran.webdesign.form;

import javax.validation.constraints.NotNull;

/**
 * 用户对应表单
 * @author anran
 *
 */
public class UserForm {

	/**
	 * 用户id
	 */
	private String uid;
	
	/**
	 * 用户账户
	 */
	private String account;
	
	/**
	 * 用户密码‘
	 */
	private String password;
	
	/**
	 * 用户姓名
	 */
	private String uname;
	
	/**
	 * 用户类型
	 */
	private String type;
	
	/**
	 * 用户手机
	 */
	private String mobile;
	
	/**
	 * 用户邮箱
	 */
	private String email;

	/**
	 * 用户状态
	 */
	private String status;
	
	
	/**
	 * 创建时间
	 */
	private String craetedTime;
	
	/**
	 * 修改时间
	 */
	private String updatedTime;
	
	
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCraetedTime() {
		return craetedTime;
	}

	public void setCraetedTime(String craetedTime) {
		this.craetedTime = craetedTime;
	}

	public String getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	
}

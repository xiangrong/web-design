package com.anran.webdesign.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;

import org.springframework.web.multipart.MultipartFile;

/**
 * 生成随机数图片
 * @author anran
 *
 */
public class RandImage {

	/**
	 * 图片高
	 */
	private int height = 40;
	
	/**
	 * 图片宽
	 */
	private int width = 85;
	
	/**
	 * 背景色
	 */
	private int rgb	= 1;
	

	/**
	 * 获取随机数图片，
	 * @param num  几位
	 * @return
	 * @throws IOException 
	 */
	public String getPandImage(int num) throws IOException{
		String url = "";
		
		//在内存中创建图片
		BufferedImage image = new BufferedImage(width, height, rgb);
		//获取图片上下文
		Graphics g = image.getGraphics();
		//生成随机数
		Random random = new Random();
		//设定背景色
		g.setColor(getRandColor(200, 250));
		g.fillRect(0, 0, width, height);
		//设定字体
		g.setFont(new Font("Times New Roman", 0, 18));
		//随机生成10条干扰线，使验证码不易被看出来
		g.setColor(getRandColor(160,200));
		for(int i=0; i<10; i++){
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			int x1 = random.nextInt(12);
			int y1 = random.nextInt(12);
			g.drawLine(x, y, x+x1, y+y1);
		}
		//取随机产生num验证码
		for(int i=0; i<num; i++){
			String rand = String.valueOf(random.nextInt(10));
			g.setColor(new Color(20+random.nextInt(110),20+random.nextInt(110),20+random.nextInt(110)));
			g.drawString(rand,13*i+6,16);
		}
		//图像生效
		g.dispose();
		String fileName = System.currentTimeMillis()+ ".JPEG";
		File file = new File("/tmp/picture/"+fileName);
        ImageIO.write(image, "JPEG", file);
        
        url = "H:\\tmp\\picture\\"+fileName;
		return url;
	}
	
	/*
	 * 给定范围获得随机颜色
	 */
	private Color getRandColor(int fc,int bc){
        Random random = new Random();
        if(fc>255) fc=255;
        if(bc>255) bc=255;
        int r=fc+random.nextInt(bc-fc);
        int g=fc+random.nextInt(bc-fc);
        int b=fc+random.nextInt(bc-fc);
        return new Color(r,g,b);
   }
	
	
	public static void main(String[] args) throws IOException {
		RandImage image = new RandImage();
		String url = image.getPandImage(6);
		System.out.println("url = " + url);
		
		
//		测试一下随机数,得出nextInt(),是随机整数，nextInt(n),是从0到n的整数
//		Random random = new Random();
//		for(int i=0; i<100; i++){
//			System.out.println(random.nextInt());
//		}
//		System.out.println("以上是nextInt(),下面是nextInt(n)");
//		for(int i=0; i<100; i++){
//			System.out.println(random.nextInt(56));
//		}
	}
}

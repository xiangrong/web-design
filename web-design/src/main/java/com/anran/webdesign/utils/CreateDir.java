package com.anran.webdesign.utils;

import java.io.File;

/**
 * 创建文件夹工具类
 * @author anran
 *
 */
public class CreateDir {

	public boolean makeDir(String filePath){
		boolean result = false;
		
		if(CommonUtil.isEmpty(filePath)){
			return false;
		}
		
		File file = new File(filePath);
		file.mkdirs();
		
		return result;
	}
	
	
	public static void main(String[] args) {
		CommonUtil.makeDir("/tmp/ssd/");
		System.out.println("finsh");
	}
}

package com.anran.webdesign.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anran.webdesign.constant.ConfigConstant;
import com.anran.webdesign.constant.GlobalConstant;
import com.google.gson.JsonObject;


/**
 * 短信发送工具类
 * @author anran
 *
 */
public class SmsUtil {

	private static final Logger log = LoggerFactory.getLogger(SmsUtil.class);
	
	/**
	 * 发送短信验证码(针对云之讯公司)
	 * @param mobile
	 * @param content(多个参数以英文逗号隔开，次数使用验证码{1}，在{2}分钟内有效，相似的模板)
	 * @return
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	public static boolean send(String mobile,String content ) {
		
		log.info("发送个短信验证码：mobile:"+mobile+",content:"+content);
		
		String time = (new SimpleDateFormat("yyyyMMddHHmmssSSS")).format(new Date());
		String sign = MD5Util.MD5(ConfigConstant.SMS_SID+time+ConfigConstant.SMS_TOKEN);
		StringBuilder url = new StringBuilder();
		url.append(ConfigConstant.SMS_URL).append("?");
		url.append("sid=").append(ConfigConstant.SMS_SID);
		url.append("&appId=").append(ConfigConstant.SMS_APPID);
		url.append("&time=").append(time);
		url.append("&sign=").append(sign);
		url.append("&to=").append(mobile);
		url.append("&templateId=").append(ConfigConstant.SMS_TEMPLATEID);
		url.append("&param=").append(content);
		
		log.info("url:"+url);
		
		HttpUtil httpHelper = new HttpUtil();
		
		String result = "";
		
//		String result = "{\"resp\":{\"respCode\":\"000000\",\"templateSMS\":{\"createDate\":\"20151029095042\",\"smsId\":\"7ebd9297786640e5abefd9b555174409\"}}}";
		try {
			result = httpHelper.doPost(url.toString(), GlobalConstant.Encoding.UTF8);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		log.info("result:"+result);
		
		
		//获取结果中返回码的方法1
		JSONObject resultjson1 = JSONObject.fromObject(result);
		log.info(resultjson1.getString("resp"));
		JSONObject resultjson12= JSONObject.fromObject(resultjson1.getString("resp"));
		log.info(resultjson12.getString("respCode"));
		
//		//获取结果中返回码的方法2
//		ObjectMapper mapper = new ObjectMapper();
//		JsonNode resultJson = mapper.readTree(result);
//		System.out.println(resultJson.get("resp").get("respCode"));
		
		if(resultjson12.getString("respCode").equals(GlobalConstant.Sms.SEND_SUCCESS)){
			return true;
		}else{
			return false;
		}
	}
	
	
	public static void main(String[] args) {
		SmsUtil util = new SmsUtil();
		util.send("18829572665", "31332,10");
		
	}
}

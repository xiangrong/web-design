package com.anran.webdesign.utils;

import com.anran.webdesign.constant.GlobalConstant;

/**
 * json数据返回抽象类
 * @author anran
 *
 */
public abstract class AbstractJsonResponse {

	public String respCode = GlobalConstant.Respond.CODE_SUCCESS;
	public String respMessage = GlobalConstant.Respond.MESSAGE_SUCCESS;
}

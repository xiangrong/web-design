package com.anran.webdesign.utils;

import org.apache.log4j.Logger;
import java.io.Serializable;
import java.lang.reflect.Method;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.Assigned;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.UUIDHexGenerator;

public class CreateUuid implements IdentifierGenerator {
	private static final Logger logger = Logger.getLogger(CreateUuid.class);

	public Serializable generate(SessionImplementor arg0, Object arg1)
			throws HibernateException {
		Class<?> cla = arg1.getClass();
		Object id = null;
		try {
			Method met = cla.getMethod("getId", new Class[] {});
			id = met.invoke(arg1, new Object[] {});
		} catch (Exception e) {
			logger.error("generate(SessionImplementor, Object)", e); //$NON-NLS-1$

			e.printStackTrace();
		}
		if (id == null || "".equals(id)) {
			UUIDHexGenerator ad = new UUIDHexGenerator();
			return ad.generate(arg0, arg1);
		} else {
			Assigned as = new Assigned();
			return as.generate(arg0, arg1);
		}

	}

}

package com.anran.webdesign.utils;


import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.anran.webdesign.constant.ConfigConstant;



/**
 * 邮件发送工具类
 * @author anran
 *
 */
public class EmailUtil {

		private static final Log log = LogFactory.getLog(EmailUtil.class);
		private static InternetAddress[] internetAddresses;
		
		
		public static void sendEmails(String title, String to, String content) {

			log.info("发送邮件信息：to："+to+".title:"+title+",content:"+content);
			
			getInternetAddress(to);
			//创建配置文件对象
			Properties props = new Properties();
			//设置验证机制，即账号和密码验证
			props.put("mail.smtp.auth", "true");
//			 props.put("mail.smtp.starttls.enable", "true");
			//设置服务器地址
			props.put("mail.smtp.host", "smtp.sina.com");
			//设置服务器端口号
			props.put("mail.smtp.port", "25");

			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
//					return new PasswordAuthentication("lxr_anjiula@126.com", "orpwofqiiqlvpprz");
//					return new PasswordAuthentication("lxr_anran@126.com", "puwlpxrydzqhikuw");
					return new PasswordAuthentication("lxr_anjiula@sina.com", "ajl930919");
				}
			});

			try {

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(ConfigConstant.EMAIL_ADDRESS));
				message.setRecipients(Message.RecipientType.TO, internetAddresses);
				message.setSubject(title);
				message.setText(content);
				Transport.send(message);
				log.info("done");

			} catch (MessagingException e) {
				throw new RuntimeException(e);
			} 
		}
		
		public static void getInternetAddress(String to){

			
			String sendTo = to;
			if (!"".equals(sendTo)) {
				String[] addresses = sendTo.split(";");
				internetAddresses = new InternetAddress[addresses.length];
				for (int i = 0; i < addresses.length; i++) {
					InternetAddress addr = null;
					try {
						addr = new InternetAddress(addresses[i]);
					} catch (AddressException e) {
						e.printStackTrace();
					}
					if (addr != null)
						internetAddresses[i] = addr;
				}
			}
		}
		
		public static void main(String[] args) {
			// TODO Auto-generated method stub

			EmailUtil email = new EmailUtil();
			email.sendEmails("欢迎注册","460149790@qq.com;liuxiangrong@funguide.com.cn","请点击下面链接完成注册");
		}

	}


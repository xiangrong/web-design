package com.anran.webdesign.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.DefaultPropertiesPersister;
import org.springframework.util.PropertiesPersister;

import com.anran.webdesign.constant.GlobalConstant;

/**
 * Properties配置文件工具类.
 * 
 * @author anran
 */
public class PropertiesUtil {

	private static final Log log = LogFactory.getLog(PropertiesUtil.class);
	
	private static PropertiesPersister propertiesPersister = new DefaultPropertiesPersister();
	
	private static ResourceLoader resourceLoader = new DefaultResourceLoader();

	public static Properties loadProperties(String... resourcesPaths)
			throws IOException {
		Properties props = new Properties();
		for (String location : resourcesPaths) {
			log.debug("Loading properties file from:" + location);
			InputStream is = null;
			try {
				Resource resource = resourceLoader.getResource(location);
				is = resource.getInputStream();
				propertiesPersister.load(props, new InputStreamReader(is, GlobalConstant.Encoding.UTF8));
			} catch (IOException ex) {
				log.info("Could not load properties from classpath:" + location + ": " + ex.getMessage());
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}
		return props;
	}
}

package com.anran.webdesign.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 文章类型
 * @author anran
 *
 */
@Entity
@Table(name="article_type",schema="public")
public class ArticleType {

	@Id
	@GeneratedValue(generator = "article_type")
	@GenericGenerator(name="article_type", strategy="com.anran.webdesign.utils.CreateUuid")
	@Column(name = "tid")
	private String id;
	
	/**
	 * 类型名称
	 */
	@Column(name="type_name")
	private String typeName;
	
	/**
	 * 作者
	 */
	@Column(name="author")
	private String author;
	
	/**
	 * 创建时间
	 */
	@Column(name="created_time")
	private Date createdTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="updated_time")
	private Date updatedTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
}

package com.anran.webdesign.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 文章评论
 * @author anran
 *
 */
@Entity
@Table(name="article_comment",schema="public")
public class ArticleComment {

	@Id
	@GeneratedValue(generator = "article_comment")
	@GenericGenerator(name="article_comment", strategy="com.anran.webdesign.utils.CreateUuid")
	@Column(name = "acid")
	private String id;
	
	/**
	 * 文章id
	 */
	@Column(name="article_id")
	private String articleId;
	
	/**
	 * 评论人id
	 */
	@Column(name="author_id")
	private String authorId;
	
	/**
	 * 评论人姓名
	 */
	@Column(name="author_name")
	private String authorName;
	
	/**
	 * 被评论人id
	 */
	@Column(name="to_author_id")
	private String toAuthorId;
	
	/**
	 * 被评论人名称
	 */
	@Column(name="to_author_name")
	private String toAuthorName;
	
	/**
	 * 被评论人是否查看
	 */
	@Column(name="is_see")
	private String isSee;
	
	/**
	 * 创建时间
	 */
	@Column(name="created_time")
	private Date createdTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="updated_time")
	private Date updatedTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public String getAuthorId() {
		return authorId;
	}

	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getToAuthorId() {
		return toAuthorId;
	}

	public void setToAuthorId(String toAuthorId) {
		this.toAuthorId = toAuthorId;
	}

	public String getToAuthorName() {
		return toAuthorName;
	}

	public void setToAuthorName(String toAuthorName) {
		this.toAuthorName = toAuthorName;
	}

	public String getIsSee() {
		return isSee;
	}

	public void setIsSee(String isSee) {
		this.isSee = isSee;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	
}

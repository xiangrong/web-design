package com.anran.webdesign.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 文章内容
 * @author anran
 *
 */
@Entity
@Table(name="article_content",schema="public")
public class ArticleContent {

	@Id
	@GeneratedValue(generator = "article_content")
	@GenericGenerator(name="article_content", strategy="com.anran.webdesign.utils.CreateUuid")
	@Column(name = "acid")
	private String id;
	
	/**
	 * 文章的id
	 */
	@Column(name="article_id")
	private String articleId;
	
	/**
	 * 文章周中的顺序
	 */
	@Column(name="sequence")
	private int sequence;
	
	/**
	 * 内容
	 */
	@Column(name="content",length=1000)
	private String content;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}

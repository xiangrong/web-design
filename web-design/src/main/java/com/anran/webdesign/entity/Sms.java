package com.anran.webdesign.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.sun.org.apache.bcel.internal.generic.DADD;


/**
 * 短信验证码实体
 * @author anran
 *
 */
@Entity
@Table(name="sms",schema="public")
public class Sms {

	@Id
	@GeneratedValue(generator = "sms")
	@GenericGenerator(name="sms", strategy="com.anran.webdesign.utils.CreateUuid")
	@Column(name = "sid")
	private String id;
	
	/**
	 * 用户手机号
	 */
	@Column(name="mobile",length=11)
	private String mobile;
	
	/**
	 * 验证码
	 */
	@Column(name="code",length=6)
	private String code;
	
	/**
	 * 状态，0未使用。1已使用
	 */
	@Column(name="status",length=1)
	private String status;
	
	/**
	 * 创建时间
	 */
	@Column(name="created_time")
	private Date createdTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="updated_time")
	private Date updatedTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	
}

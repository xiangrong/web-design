package com.anran.webdesign.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 文章信息
 * @author anran
 *
 */
@Entity
@Table(name="article_info",schema="public")
public class ArticleInfo {

	@Id
	@GeneratedValue(generator = "article_info")
	@GenericGenerator(name="article_info", strategy="com.anran.webdesign.utils.CreateUuid")
	@Column(name = "aiid")
	private String id;
	
	/**
	 * 作者
	 */
	@Column(name="author")
	private String author;
	
	/**
	 * 文章类型
	 */
	@Column(name="content_type")
	private String contentType;
	
	/**
	 * 显示类型
	 */
	@Column(name="show_type")
	private String showType;
	
	/**
	 * 摘要
	 */
	@Column(name="summary")
	private String summary;
	
	/**
	 * 文章包含多少个内容
	 */
	@Column(name="lengeth")
	private int length;
	
	/**
	 * 创建时间
	 */
	@Column(name="created_time")
	private Date createdTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="updated_time")
	private Date updatedTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getShowType() {
		return showType;
	}

	public void setShowType(String showType) {
		this.showType = showType;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
}

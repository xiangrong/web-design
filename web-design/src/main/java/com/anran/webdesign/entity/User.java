package com.anran.webdesign.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;


/**
 * 用户对应实体
 * @author anran
 *
 */
@Entity
@Table(name="user",schema="public")
public class User {

	/**
	 * 用户系统生成32为id
	 */
	@Id										//指定唯一主键
	@GeneratedValue(generator = "user")		//指定创建主键的方式
	@GenericGenerator(name = "user", strategy = "com.anran.webdesign.utils.CreateUuid") //指定设置主键实现的方法
	@Column(name = "uid", length = 32)
	private String id;
	
	/**
	 * 用户注册的账户
	 */
	@Column(name="account")
	private String account;
	
	
	/**
	 * 用户姓名
	 */
	@Column(name="uname")
	private String uname;
	
	/**
	 * 密码
	 */
	@Column(name="password")
	private String password;
	
	/**
	 * 用户类型
	 */
	@Column(name="type",length=1)
	private String type;
	
	
	/**
	 * 用户手机
	 */
	@Column(name="mobile")
	private String mobile;
	
	
	/**
	 * 用户邮箱
	 */
	@Column(name="email")
	private String email;

	/**
	 * 用户状态
	 */
	@Column(name="status",length=1)
	private String status;
	

	/**
	 * 创建时间
	 */
	@Column(name="created_time")
	private Date createdTime;

	/**
	 * 修改时间
	 */
	@Column(name="updated_time")
	private Date updatedTime;
	
	
	@Column(name="salt")
	private String salt;
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getAccount() {
		return account;
	}


	public void setAccount(String account) {
		this.account = account;
	}


	public String getUname() {
		return uname;
	}


	public void setUname(String uname) {
		this.uname = uname;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getCreatedTime() {
		return createdTime;
	}


	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}


	public Date getUpdatedTime() {
		return updatedTime;
	}


	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}


	public String getSalt() {
		return salt;
	}


	public void setSalt(String salt) {
		this.salt = salt;
	}
	
	
}

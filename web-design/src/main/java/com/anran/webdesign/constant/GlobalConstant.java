package com.anran.webdesign.constant;


/**
 * 全局常量控制类
 * @author anran
 *
 */
public class GlobalConstant {

	
	/**
	 * 通用参数
	 */
	public static final class Common{
		/**
		 * 用户加密时盐的长度
		 */
		public static final int SALT_LENGTH = 12;
	}
	
	/**
	 * 接口应答常用参数
	 * @author anran
	 *
	 */
	public static final class Respond{
		public static final String CODE_SUCCESS = "0000";
		public static final String CODE_ERROR = "9000";
		public static final String MESSAGE_SUCCESS = "请求成功";
		public static final String MESSAGE_ERROR = "请求失败";
	}
	
	/**
	 * 用户信息常用参数
	 * @author anran
	 *
	 */
	public static final class User{
		/**
		 * 用户身份为普通用户
		 */
		public static final String TYPE_COMMON = "1";
		/**
		 * 用户身份为管理员
		 */
		public static final String TYPE_MANAGE = "2";
		
		/**
		 * 账户已被激活
		 */
		public static final String STATUS_USE = "1";
		
		public static final String STATUS_NO_USE = "0";
	}

	/**
	 * 字符编码类型
	 * 
	 * @author anran
	 *
	 */
	public static final class Encoding {
		public static final String UTF8 = "UTF-8";
		public static final String GBK = "GBK";
	}
	
	/**
	 * 邮件常用参数
	 * @author anran
	 *
	 */
	public static final class Email{
		
		/**
		 * 注册邮件标题推送术语
		 */
		public static final String REGIST_MESSAGE_TITLE = "WEB-DESIGN 注册链接";
		/**
		 * 注册邮件内容推送术语
		 */
		public static final String REGIST_MESSAGE_CONTENT = "欢迎注册WEB-DESIGN，（如非本人操作请忽略）请点击下面后面链接完成注册，链接地址："+ConfigConstant.DOMAIN+"jsp/email_check.jsp?uid=";
		
	}
	
	/**
	 * 返回客户端链接（domain中以及指出到项目路径）
	 * @author anran
	 *
	 */
	public static final class Url{
		
		/**
		 * 请求链接为前往登录页面
		 */
		public static final String GO_LOGIN = "login.jsp";
	}
	
	
	/**
	 * 短信常用参数
	 * @author anran
	 *
	 */
	public static final class Sms{
		
		/**
		 * 短信发送成功标识
		 */
		public static final String SEND_SUCCESS = "000000";
		
		/**
		 * 生成验证码的长度
		 */
		public static final int CODE_LENGTH = 6;
		
		
		/**
		 * 验证码的有效时间,用于信息传输
		 */
		public static final String CODE_TIME = "10";
		
		/**
		 * 验证码的有效时间，用户系统进行计算
		 */
		public static final String CODE_TIME_USE = "600000";
		
		/**
		 * 验证码状态未使用
		 */
		public static final String CODE_STATUS_NO_USE = "0";
		
		/**
		 * 验证码状态已使用
		 */
		public static final String CODE_STATUS_USE = "1";
	}
	
	/**
	 * 上传文件的一些基本参数
	 */
	public static final class upload{
		/**
		 * 文件上传最大值
		 */
		public static final long FILE_MAX_SIZE = 1*1204*1024;
		
		/**
		 * 文件保存路径
		 */
		public static final String FILE_PATH = "/tmp/s3/";
		
		/**
		 * 图片上传成功
		 */
		public static final int SUCCESS = 0;
		
		public static final int ERROR = 9;
	}
}

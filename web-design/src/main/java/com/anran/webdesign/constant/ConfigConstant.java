package com.anran.webdesign.constant;

import java.io.IOException;
import java.util.Properties;

import com.anran.webdesign.utils.PropertiesUtil;

/**
 * 常量控制类
 * @author anran
 *
 */
public class ConfigConstant {

	public static final String DOMAIN;
	public static final String EMAIL_ADDRESS;
	public static final String EMAIL_PASSWORD;
	public static final String SMS_URL;
	public static final String SMS_SID;
	public static final String SMS_TOKEN;
	public static final String SMS_APPID;
	public static final String SMS_TEMPLATEID;
	
	
	static{
		Properties properties = null;
		try {
			properties = PropertiesUtil.loadProperties("config.properties");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		DOMAIN = "http://localhost:8080/web-design/";
		EMAIL_ADDRESS = properties.getProperty("email_address");
		EMAIL_PASSWORD = properties.getProperty("email_password");
		
		SMS_URL = properties.getProperty("sms_url");
		SMS_SID = properties.getProperty("sms_sid");
		SMS_TOKEN = properties.getProperty("sms_token");
		SMS_APPID = properties.getProperty("sms_appid");
		SMS_TEMPLATEID = properties.getProperty("sms_templatedid");
	}
}

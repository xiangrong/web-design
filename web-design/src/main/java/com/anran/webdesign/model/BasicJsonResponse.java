package com.anran.webdesign.model;

import com.anran.webdesign.utils.AbstractJsonResponse;

/**
 * 基础返回信息，只包含code和message
 * @author anran
 *
 */
public class BasicJsonResponse extends AbstractJsonResponse {

}

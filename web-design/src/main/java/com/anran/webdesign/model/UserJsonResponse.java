package com.anran.webdesign.model;

import com.anran.webdesign.constant.ConfigConstant;
import com.anran.webdesign.entity.User;
import com.anran.webdesign.utils.AbstractJsonResponse;

/**
 * 用户登录返回json信息
 * @author anran
 *
 */
public class UserJsonResponse extends AbstractJsonResponse {

	public String url;
	public User user;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = ConfigConstant.DOMAIN+url;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}

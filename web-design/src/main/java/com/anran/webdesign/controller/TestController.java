package com.anran.webdesign.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.anran.webdesign.utils.HttpUtil;

@Controller(value="test")
@Scope("prototype")
@RequestMapping(value="test")
public class TestController {

	 @RequestMapping(value = "/test.json", method = {RequestMethod.GET,RequestMethod.POST})
	 @ResponseBody
	 public String callback(HttpServletRequest request,HttpServletResponse response) {
		 System.out.println("请求接口");
		 try {
				response.getWriter().print("Plain=" + "keyi");
				response.getWriter().print("\n");
				response.getWriter().print("Signature=" + "chenggong");
			} catch (IOException e) {
				e.printStackTrace();
			}
		 System.out.println("wancheng");
     	return null;
	 }
	 
	 
	 @RequestMapping(value = "/test1.json", method = {RequestMethod.GET,RequestMethod.POST})
	 @ResponseBody
	 public String callback1(HttpServletRequest request,HttpServletResponse response) throws IOException {
		System.out.println("获取信息");
		request.setAttribute("Plain", "merchantId=365010001008~|~orderId=20151223135118_CXPBLY~|~transAmt=0.02~|~transDateTime=20151223135119~|~currencyType=01~|~productInfo=海飞丝×1=0.02~|~transSeqNo=000491650024~|~ppDateTime=20151223~|~clearingDate=20151223~|~respCode=AAAAAAA~|~msgExt=~|~");
		request.setAttribute("Signature", "a236e2c20eb855749fccfbfe01d5c853bd941db3df67d117a8b93e26c5582814ccfa4c30f3a0eda62ad77206c600666a692790ecf207e1c2f4b1c3adf73ae993e7558fdf01ed4be7452ed5bd1a20ea36580cc4923ccc4e9db3b024c88fd02a9a6def3b23801e2f4d894fce71383795c96ef2131bbc57f7fb9e741543836e6bb1");
//		String result = util.doPost("http://localhost:8080/web-design/test/test.json");
//		System.out.println("获取到的信息："+result);
		
		System.out.println("我就是想休息休息，不想做太多东西");
		System.err.println("我自认为我说话很和气");
		System.err.println("但是和他就是说不到一起");
		System.out.println("话太多，说不过咱们就躲这点，不至于跟哪种人较劲，好累");
		
		boolean n =true;
		while(n){
			System.out.println("说好的不去想这些");
			System.out.println();
		}
		return  "";
	 }
}

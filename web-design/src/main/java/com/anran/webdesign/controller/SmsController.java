package com.anran.webdesign.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.Position.Bias;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.anran.webdesign.form.SmsForm;
import com.anran.webdesign.form.UserForm;
import com.anran.webdesign.model.BasicJsonResponse;
import com.anran.webdesign.service.SmsService;
import com.sun.swing.internal.plaf.basic.resources.basic;

/**
 * 短信控制类
 * @author anran
 *
 */
@Controller(value="sms")
@Scope("prototype")
@RequestMapping(value="sms")
public class SmsController {

	private static final Logger log = LoggerFactory.getLogger(SmsController.class);
	
	@Autowired
	private SmsService smsService;
	
	/**
	 * 获取短信验证码接口
	 * @param smsForm
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="getSmsCode.json", method={RequestMethod.GET,RequestMethod.POST}, produces={MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public BasicJsonResponse getSmsCode(SmsForm smsForm, HttpServletRequest request, HttpServletResponse response){
	
		log.info("获取验证码，mobile:"+smsForm.getMobile());
		
		BasicJsonResponse bj = new BasicJsonResponse();
		
		bj = smsService.getSmsCode(smsForm.getMobile());
		
		log.info("结果："+bj.respCode+",说明："+bj.respMessage);
		
		return bj;
	}

	
	/**
	 * 验证短信验证码接口
	 * @param smsForm
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="checkSmsCode.json", method={RequestMethod.GET,RequestMethod.POST}, produces={MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public BasicJsonResponse checkSmsCode(SmsForm smsForm, HttpServletResponse response, HttpServletRequest request){
		
		log.info("验证验证码：mobile:"+smsForm.getMobile()+",code:"+smsForm.getCode());
		
		BasicJsonResponse bj = new BasicJsonResponse();
		
		bj = smsService.checkSmsCode(smsForm.getMobile(), smsForm.getCode());
		
		log.info("结果："+bj.respCode+",说明："+bj.respMessage);
		
		return bj;
	}
	
}

package com.anran.webdesign.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.anran.webdesign.entity.User;
import com.anran.webdesign.form.UserForm;
import com.anran.webdesign.model.BasicJsonResponse;
import com.anran.webdesign.model.UserJsonResponse;
import com.anran.webdesign.service.UserService;
import com.anran.webdesign.utils.AbstractJsonResponse;


/**
 * 用户控制类
 * @author anran
 *
 */
@Controller
@Scope("prototype")
@RequestMapping("user")
public class UserController {

	private static final Logger log = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	/**
	 * 用户登录接口
	 * @param userForm
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="login.json", method={RequestMethod.POST, RequestMethod.GET}, produces={MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public UserJsonResponse login(UserForm userForm, HttpServletRequest request, HttpServletResponse response){
		
		log.info("用户身份验证，account:"+userForm.getAccount()+",password:"+userForm.getPassword()+",type"+userForm.getType());
		
		UserJsonResponse uj = new UserJsonResponse();
		
		uj = userService.login(userForm);
		
		log.info("结果：code:"+uj.respCode+",说明："+uj.respMessage);
		return uj;
	}
	
	
	/**
	 * 用户注册接口
	 * @param userForm
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="regist.json", method={RequestMethod.POST, RequestMethod.GET}, produces={MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public UserJsonResponse regist(UserForm userForm, HttpServletRequest request,HttpServletResponse response){
		
		log.info("创建用户，account:"+userForm.getAccount()+",password:"+userForm.getPassword()+",type:"+
				userForm.getType()+",uname:"+userForm.getUname()+",mobile:"+userForm.getMobile()+".email:"+
				userForm.getEmail());
		
		UserJsonResponse uj = new UserJsonResponse();
		
		uj = userService.regist(userForm);
		
		log.info("结果：code:"+uj.respCode+",说明："+uj.respMessage);
		return uj;
	}
	
	/**
	 * 账户邮箱验证接口
	 * @param userForm
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="emailCheck.json", method={RequestMethod.GET,RequestMethod.POST},produces={MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public UserJsonResponse emailCheck(UserForm userForm, HttpServletRequest request,HttpServletResponse response){
		log.info("账号邮箱验证，uid："+userForm.getUid());
		
		UserJsonResponse uj = new UserJsonResponse();
		
		uj = userService.emailCheck(userForm);
		
		log.info("结果：code:"+uj.respCode+",说明："+uj.respMessage);
		return uj;
	}
}

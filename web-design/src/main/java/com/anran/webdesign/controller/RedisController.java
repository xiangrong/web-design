package com.anran.webdesign.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.anran.webdesign.form.SmsForm;
import com.anran.webdesign.model.BasicJsonResponse;
import com.anran.webdesign.utils.RedisUtil;

/**
 * redis测试接口
 * @author anran
 *
 */
@Controller
@Scope("prototype")
@RequestMapping(value="redis")
public class RedisController {

	private static final Logger log = LoggerFactory.getLogger(RedisController.class);
	
	@Autowired
	private RedisUtil redisUtil;
	
	@RequestMapping(value="useRedis.json", method={RequestMethod.GET,RequestMethod.POST}, produces={MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public BasicJsonResponse getSmsCode( HttpServletRequest request, HttpServletResponse response) throws InterruptedException{
		
		log.info("进入redis 请求接口");
		
		BasicJsonResponse bj = new BasicJsonResponse();
		
		 String ping = redisUtil.ping();//测试是否连接成功,连接成功输出PONG  
		 System.out.println(ping);  
		  
		//首先,我们看下redis服务里是否有数据  
	    long dbSizeStart = redisUtil.dbSize();  
	    System.out.println(dbSizeStart);  
	  
	    redisUtil.set("username", "oyhk");//设值(查看了源代码,默认存活时间30分钟)  
	    String username = redisUtil.get("username");//取值   
	    System.out.println(username);  
	    redisUtil.set("username1", "oyhk1", 1);//设值,并且设置数据的存活时间(这里以秒为单位)  
	    String username1 = redisUtil.get("username1");  
	    System.out.println(username1);  
	    Thread.sleep(2000);//我睡眠一会,再去取,这个时间超过了,他的存活时间  
	    String liveUsername1 = redisUtil.get("username1");  
	    System.out.println(liveUsername1);//输出null  
	  
	    //是否存在  
	    boolean exist = redisUtil.exists("username");  
	    System.out.println(exist);  
	  
	    //查看keys  
	    Set<String> keys = redisUtil.keys("*");//这里查看所有的keys  
	    System.out.println(keys);//只有username username1(已经清空了)  
	  
	    //删除  
	    redisUtil.set("username2", "oyhk2");  
	    String username2 = redisUtil.get("username2");  
	    System.out.println(username2);  
	    redisUtil.del("username2");  
	    String username2_2 = redisUtil.get("username2");  
	    System.out.println(username2_2);//如果为null,那么就是删除数据了  
	  
	    //dbsize  
	    long dbSizeEnd = redisUtil.dbSize();  
	    System.out.println(dbSizeEnd);  
	  
	    //清空reids所有数据  
	    //redisService.flushDB();        
		
		log.info("结果："+bj.respCode+",说明："+bj.respMessage);
		
		return bj;
	}
}

package com.anran.webdesign.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.anran.webdesign.constant.GlobalConstant;
import com.anran.webdesign.utils.CommonUtil;
import com.anran.webdesign.utils.UploadJsonResponse;

/**
 * 后台图片上传控制
 * 
 * @author zhengmingcheng
 */
@Controller
@Scope("prototype")
@RequestMapping(value="upload")
public class UploadImageController  {

	private static final Log log = LogFactory.getLog(UploadImageController.class);

	@RequestMapping(value="image.json", method={RequestMethod.GET,RequestMethod.POST}, produces={MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public UploadJsonResponse uploadFile(HttpServletRequest request,
			@RequestParam(value = "imgFile", required = true) MultipartFile file) {
		log.info("开始文件上传");
		UploadJsonResponse uj = new UploadJsonResponse();
		System.out.println(file.getContentType());
		if (!file.getContentType().startsWith("image/") && !file.getContentType().startsWith("application/")) {
			uj.error = GlobalConstant.upload.ERROR;
			uj.message = "上传文件不是图片";
			return uj;
		}
		log.info("文件大小："+file.getSize());
		if (file.getSize() > GlobalConstant.upload.FILE_MAX_SIZE) {
			uj.error = GlobalConstant.upload.ERROR;
			uj.message = "上传图片大小，大于1M";
			return uj;
		}

		if (!file.isEmpty()) {
			String fileName = System.currentTimeMillis() + "."
					+ CommonUtil.getFileType(file.getOriginalFilename());
			File targetFile = new File(GlobalConstant.upload.FILE_PATH,
					fileName);
			try {
				file.transferTo(targetFile);
//				图片加水印
//				ImageMarkLogoUtil.markImageByText("/srv/jetty9/webapps/logo-text.png", ConfigConstant.UPLOAD_FILE_PATH + fileName, Color.white);
//				将服务器图片上传至亚马逊s3
//				AwsS3Util.uploadImage("image/" + fileName, targetFile);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String url = GlobalConstant.upload.FILE_PATH + fileName;
			uj.error = GlobalConstant.upload.SUCCESS;
			uj.url = url;
			uj.url = "http://img.fun-guide.mobi/180/ad.funguide.com.cn/elive/image/community_manage/1440479999208.jpg";
			log.info(uj);
			return uj;
			
		}
		uj.error = GlobalConstant.upload.ERROR;
		uj.message = "文件上传出现异常";
		return uj;
	}
	
}

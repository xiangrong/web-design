package com.anran.webdesign.service.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.anran.webdesign.entity.ArticleContent;
import com.anran.webdesign.service.ArticleContentService;
import com.anran.webdesign.utils.PageInfo;

@SuppressWarnings("unchecked")
@Service
public class ArticleContentServiceImpl extends BaseServiceImpl<ArticleContent> implements
		ArticleContentService {
	
	private static final Logger log = LoggerFactory.getLogger(ArticleContentServiceImpl.class);

}

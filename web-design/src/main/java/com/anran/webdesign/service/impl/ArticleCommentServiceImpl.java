package com.anran.webdesign.service.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.anran.webdesign.entity.ArticleComment;
import com.anran.webdesign.service.ArticleCommentService;
import com.anran.webdesign.utils.PageInfo;

@SuppressWarnings("unchecked")
@Service
public class ArticleCommentServiceImpl extends BaseServiceImpl<ArticleComment> implements
		ArticleCommentService {

	private static final Logger log = LoggerFactory.getLogger(ArticleCommentServiceImpl.class);

}

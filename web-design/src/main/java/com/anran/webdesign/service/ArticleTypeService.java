package com.anran.webdesign.service;

import com.anran.webdesign.entity.ArticleType;

public interface ArticleTypeService extends BaseService<ArticleType> {

}

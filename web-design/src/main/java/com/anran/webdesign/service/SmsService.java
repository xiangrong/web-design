package com.anran.webdesign.service;

import com.anran.webdesign.entity.Sms;
import com.anran.webdesign.model.BasicJsonResponse;

public interface SmsService extends BaseService<Sms> {

	/**
	 * 获取验证码
	 * @param mobile
	 * @return
	 */
	public BasicJsonResponse getSmsCode(String mobile);
	
	
	/**
	 * 验证验证码
	 * @param mobule
	 * @param code
	 * @return
	 */
	public BasicJsonResponse checkSmsCode(String mobile, String code);
}

package com.anran.webdesign.service;

import com.anran.webdesign.entity.User;
import com.anran.webdesign.form.UserForm;
import com.anran.webdesign.model.UserJsonResponse;

public interface UserService extends BaseService<User> {

	/**
	 * 用户登录
	 * @param userForm
	 * @return
	 */
	public UserJsonResponse login(UserForm userForm);
	
	
	/**
	 * 注册用户
	 * @param userForm
	 * @return
	 */
	public UserJsonResponse regist(UserForm userForm);
	
	
	/**
	 * 账户通过邮箱验证
	 * @param userForm
	 * @return
	 */
	public UserJsonResponse emailCheck(UserForm userForm);
}

package com.anran.webdesign.service.impl;

import java.util.List;
import java.util.Map;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.anran.webdesign.entity.ArticleType;
import com.anran.webdesign.service.ArticleTypeService;
import com.anran.webdesign.utils.PageInfo;

@SuppressWarnings("unchecked")
@Service
public class ArticleTypeServiceImpl extends BaseServiceImpl<ArticleType> implements
		ArticleTypeService {

	private static final Logger log = LoggerFactory.getLogger(ArticleTypeServiceImpl.class);
}

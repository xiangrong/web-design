package com.anran.webdesign.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.anran.webdesign.constant.ConfigConstant;
import com.anran.webdesign.constant.GlobalConstant;
import com.anran.webdesign.entity.User;
import com.anran.webdesign.form.UserForm;
import com.anran.webdesign.model.UserJsonResponse;
import com.anran.webdesign.service.UserService;
import com.anran.webdesign.utils.CommonUtil;
import com.anran.webdesign.utils.EmailUtil;
import com.anran.webdesign.utils.MD5Util;
import com.anran.webdesign.utils.PageInfo;

@SuppressWarnings("unchecked")
@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	public UserJsonResponse login(UserForm userForm) {
		log.info("用户信息验证");
		UserJsonResponse uj = new UserJsonResponse();
		//对数据进行验证
		uj = checkLogin(userForm);
		if(GlobalConstant.Respond.CODE_ERROR.equals(uj.respCode)){
			return uj;
		}
		//判断账户是否存在
		List<User> users = this.findByProperty(User.class, "account", userForm.getAccount());
		if(users.size() == 0){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "账户不存在";
			return uj;
		}	
		log.info(MD5Util.MD5(userForm.getPassword()+users.get(0).getSalt()));
		if(!MD5Util.MD5(userForm.getPassword()+users.get(0).getSalt()).equals(users.get(0).getPassword())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "密码不正确";
			return uj;
		}
		if(users.get(0).getStatus().equals(GlobalConstant.User.STATUS_NO_USE)){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "账户未激活";
			return uj;
		}
		
		uj.respCode = GlobalConstant.Respond.CODE_SUCCESS;
		uj.respMessage = "用户信息正确";
		uj.setUser(users.get(0));
		uj.setUrl("/jsp/hello.jsp");
		return uj;
		
	} 
	
	

	@Override
	public UserJsonResponse regist(UserForm userForm) {
		log.info("用户信息注册");
		UserJsonResponse uj = new UserJsonResponse();
		uj = checkRegist(userForm);
		if(uj.respCode.equals(GlobalConstant.Respond.CODE_ERROR)){
			return uj;
		}
		List<User> list = this.findByProperty(User.class, "account", userForm.getAccount());
		if(list.size() >0 && list.get(0).getStatus().equals(GlobalConstant.User.STATUS_NO_USE)){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "该账户已被注册，但未激活，已发邮件信息到对应邮箱请激活再用";
			//重新发送注册邮件
			EmailUtil.sendEmails(GlobalConstant.Email.REGIST_MESSAGE_TITLE, userForm.getAccount(), GlobalConstant.Email.REGIST_MESSAGE_CONTENT+list.get(0).getId());
			
			return uj;
		}
		if(list.size() >0 && list.get(0).getStatus().equals(GlobalConstant.User.STATUS_USE)){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "账户已经被注册";
			return uj;
		}
		
		//生成一个12位的随机数
		String salt = CommonUtil.generateRandomNumber(GlobalConstant.Common.SALT_LENGTH);
		
		User user = new User();
		user.setAccount(userForm.getAccount());
		user.setPassword(MD5Util.MD5(userForm.getPassword()+salt));
		user.setUname(userForm.getUname());
		user.setType(userForm.getType());
		user.setMobile(userForm.getMobile());
		user.setEmail(userForm.getEmail());
		user.setStatus(GlobalConstant.User.STATUS_NO_USE);
		user.setCreatedTime(new Date());
		user.setUpdatedTime(new Date());
		user.setSalt(salt);
		
		String uid = (String) this.save(user);
		log.info("用户系统id:"+uid);
		//发送注册邮件
		EmailUtil.sendEmails(GlobalConstant.Email.REGIST_MESSAGE_TITLE, userForm.getAccount(), GlobalConstant.Email.REGIST_MESSAGE_CONTENT+uid);
		
		uj.respCode = GlobalConstant.Respond.CODE_SUCCESS;
		uj.respMessage = "注册成功,请前往注册邮箱验证";
		uj.setUser(user);
		uj.setUrl("/jsp/hello.jsp");
		return uj;
	}
	
	
	@Override
	public UserJsonResponse emailCheck(UserForm userForm) {

		log.info("账户邮箱验证");
		
		UserJsonResponse uj = new UserJsonResponse();
		
		uj = checkEmailCheck(userForm);
		uj.setUrl(GlobalConstant.Url.GO_LOGIN);
		if(uj.respCode.equals(GlobalConstant.Respond.CODE_ERROR)){
			return uj;
		}
		
		//验证账户是否存在
		User user = this.findById(User.class, userForm.getUid());
		if(CommonUtil.isEmpty(user)){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "验证的账户不存在";
			return uj;
		}
		if(user.getStatus().equals(GlobalConstant.User.STATUS_USE)){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "该账户已经验证成功，请勿重复验证";
			return uj;
		}
		
		user.setStatus(GlobalConstant.User.STATUS_USE);
		this.update(user);
		
		log.info("验证成功");
		
		uj.respCode = GlobalConstant.Respond.CODE_SUCCESS;
		uj.respMessage = "账户已经激活";
		uj.setUser(user);
		
		return uj;
				
	}
	
	
	public UserJsonResponse checkEmailCheck(UserForm userForm){
		UserJsonResponse uj = new UserJsonResponse();
		if(CommonUtil.isEmpty(userForm.getUid())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "账户id不能为空，验证失败";
			return uj;
		}
		return uj;
	}
	
	public UserJsonResponse checkRegist(UserForm userForm){
		UserJsonResponse uj = new UserJsonResponse();
		//邮箱正在表达式
		String email_regul_expression="[a-zA-Z0-9_\\-\\.]+@+[a-zA-Z0-9_\\-\\.]+(\\.(com|cn|org|edu|hk))";
		//手机号正则表达式
		String phonoe_regul_experssion="(13|15|18)+[0-9]{9}";
		if(CommonUtil.isEmpty(userForm.getAccount())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "账户不能为空";
			return uj;
		}
		if(!Pattern.matches(email_regul_expression,userForm.getAccount())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "账户格式不正确";
			return uj;
		}
		if(CommonUtil.isEmpty(userForm.getPassword())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "密码不能为空";
			return uj;
		}
		if(userForm.getPassword().length()<6 || userForm.getPassword().length()>13){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "密码长度不再6-12之内";
			return uj;
		}
		if(CommonUtil.isEmpty(userForm.getUname())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "用户名称不能为空";
			return uj;
		}
		if(CommonUtil.isEmpty(userForm.getType())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "注册用户类型不能为空";
			return uj;
		}
		if(!GlobalConstant.User.TYPE_COMMON.equals(userForm.getType()) && !GlobalConstant.User.TYPE_MANAGE.equals(userForm.getType())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "用户类型不合适";
			return uj;
		}
		return uj;
	}
	
	
	public UserJsonResponse checkLogin(UserForm userForm){
		UserJsonResponse uj = new UserJsonResponse();
		if(CommonUtil.isEmpty(userForm.getAccount())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "账户不能为空";
			return uj;
		}
		if(CommonUtil.isEmpty(userForm.getPassword())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "密码不能为空";
			return uj;
		}
		if(CommonUtil.isEmpty(userForm.getType())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "用户类型不能为空";
			return uj;
		}
		if(!GlobalConstant.User.TYPE_COMMON.equals(userForm.getType()) && !GlobalConstant.User.TYPE_MANAGE.equals(userForm.getType())){
			uj.respCode = GlobalConstant.Respond.CODE_ERROR;
			uj.respMessage = "用户类型不合适";
			return uj;
		}
		
		return uj;
	}



	
}

package com.anran.webdesign.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.LockMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.anran.webdesign.constant.GlobalConstant;
import com.anran.webdesign.entity.Sms;
import com.anran.webdesign.model.BasicJsonResponse;
import com.anran.webdesign.service.SmsService;
import com.anran.webdesign.utils.CommonUtil;
import com.anran.webdesign.utils.PageInfo;
import com.anran.webdesign.utils.SmsUtil;

@SuppressWarnings("unchecked")
@Service
public class SmsServiceImpl extends BaseServiceImpl<Sms> implements SmsService {

	private static final Logger log = LoggerFactory.getLogger(SmsServiceImpl.class);
	
	@Override
	public BasicJsonResponse getSmsCode(String mobile) {
		
		log.info("获取验证码");
		
		BasicJsonResponse bj = new BasicJsonResponse();
		
		if(CommonUtil.isEmpty(mobile)){
			bj.respCode = GlobalConstant.Respond.CODE_ERROR;
			bj.respMessage = "手机号码不能为空";
			return bj;
		}
		if(!CommonUtil.isMobile(mobile)){
			bj.respCode = GlobalConstant.Respond.CODE_ERROR;
			bj.respMessage = "手机号码格式不正确";
			return bj;
		}
		String code = CommonUtil.generateRandomNumber(GlobalConstant.Sms.CODE_LENGTH);

		List<Sms> list = this.findByProperty(Sms.class, "mobile", mobile);
		Date currDate = new Date();
		if(list.size() != 0){
			//判断上次发送时间举例现在是不是大于10分钟
			if(currDate.getTime() - list.get(0).getUpdatedTime().getTime() < Long.parseLong(GlobalConstant.Sms.CODE_TIME_USE) 
					&& list.get(0).getStatus().equals(GlobalConstant.Sms.CODE_STATUS_NO_USE)){
				bj.respCode = GlobalConstant.Respond.CODE_ERROR;
				bj.respMessage = "请勿在10分钟内，重复获取验证码";
				return bj;
			}
		}
		
		boolean result = SmsUtil.send(mobile, code+","+GlobalConstant.Sms.CODE_TIME);
		
		if(result){
			
			if(list.size() !=0){
				
				list.get(0).setCode(code);
				list.get(0).setStatus(GlobalConstant.Sms.CODE_STATUS_NO_USE);
				list.get(0).setUpdatedTime(currDate);
				
				this.update(list.get(0));
				
			}else{
				Sms sms = new Sms();
				sms.setMobile(mobile);
				sms.setCode(code);
				sms.setStatus(GlobalConstant.Sms.CODE_STATUS_NO_USE);
				sms.setCreatedTime(currDate);
				sms.setUpdatedTime(currDate);
				
				this.save(sms);
			}
			bj.respCode = GlobalConstant.Respond.CODE_SUCCESS;
			bj.respMessage = "验证码发送成功，请查收";
		}else{
			bj.respCode = GlobalConstant.Respond.CODE_ERROR;
			bj.respMessage = "验证码发送失败，请重新发送";
		}
		return bj;
	}

	@Override
	public BasicJsonResponse checkSmsCode(String mobile, String code) {

		BasicJsonResponse bj = new BasicJsonResponse();
		
		if(CommonUtil.isEmpty(mobile)){
			bj.respCode = GlobalConstant.Respond.CODE_ERROR;
			bj.respMessage = "手机号不能为空";
			return bj;
		}
		if(!CommonUtil.isMobile(mobile)){
			bj.respCode = GlobalConstant.Respond.CODE_ERROR;
			bj.respMessage = "手机号码格式不正确";
			return bj;
		}
		if(CommonUtil.isEmpty(code)){
			bj.respCode = GlobalConstant.Respond.CODE_ERROR;
			bj.respMessage = "验证码不能为空";
			return bj;
		}
		
		List<Sms> list = this.findByProperty(Sms.class, "mobile", mobile);
		
		if(list.size() == 0){
			bj.respCode = GlobalConstant.Respond.CODE_ERROR;
			bj.respMessage = "手机号码不存在";
		}else{
			if(code.equals(list.get(0).getCode()) && GlobalConstant.Sms.CODE_STATUS_NO_USE.equals(list.get(0).getStatus())){
				
				list.get(0).setStatus(GlobalConstant.Sms.CODE_STATUS_USE);
				list.get(0).setUpdatedTime(new Date());
				this.update(list.get(0));
				
				bj.respCode = GlobalConstant.Respond.CODE_SUCCESS;
				bj.respMessage = "验证成功";
			}else if(code.equals(list.get(0).getCode()) && GlobalConstant.Sms.CODE_STATUS_USE.equals(list.get(0).getStatus())){
				bj.respCode = GlobalConstant.Respond.CODE_ERROR;
				bj.respMessage = "该验证码已经被验证，请勿重复验证";
			}else{
				bj.respCode = GlobalConstant.Respond.CODE_ERROR;
				bj.respMessage = "验证码错误";
			}
		}
		return bj;
	}

	

}

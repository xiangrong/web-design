<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/jsp/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WEB-DESIGN</title>

</head>

<body>
	<div align="center">网页设计练习项目</div>
	<div>
		<div><a href="${pageContext.request.contextPath}/Slides.jsp">Slides图片显示</a></div>
		<div><a href="${pageContext.request.contextPath}/bxsider.jsp">bxsider图片显示</a></div>
		<div><a href="${pageContext.request.contextPath}/login.jsp">登录页面设计（包含登录、注册。邮件验证账号登功能）</a></div>
		<div><a href="${pageContext.request.contextPath}/login_two.jsp">登录页面设计</a></div>
		<div><a href="${pageContext.request.contextPath}/jsp/sms_check.jsp">手机验证（包含获取验证码，以及验证码验证功能）</a></div>
		<div><a href="${pageContext.request.contextPath}/jsp/editor.jsp">编辑器</a></div>
		<div><a href="${pageContext.request.contextPath}/jsp/baidumap.jsp">百度地图</a></div>
		<div><a href="${pageContext.request.contextPath}/blog/html/bolg_login.html">博客</a></div>
		
	</div>
	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common/jquery.bxslider.css"> 
<script src="${pageContext.request.contextPath}/js/common/jquery.min.js"></script> 
<script src="${pageContext.request.contextPath}/js/common/jquery.bxslider.min.js"></script>
 <script>  
 $(function(){
		$('.bxslider').bxSlider({
			mode: 'fade',
	  		captions: true
		});
	});
</script>
</head>
<body>

	<ul class="bxslider"> 
      <li><img src="images/s1.jpg" /></li> 
      <li><img src="images/s2.jpg" /></li> 
      <li><img src="images/s3.jpg" /></li> 
	</ul> 

</body>
</html>
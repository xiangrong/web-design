<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/jsp/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>editor</title>
	
	<!-- <script charset="utf-8" src="../kindeditor/kindeditor.js"></script>
	<script charset="utf-8" src="../kindeditor/lang/zh_CN.js"></script>
	 -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/kindeditor/plugins/code/prettify.css" />
	<script charset="utf-8" src="${pageContext.request.contextPath}/kindeditor/kindeditor.js"></script>
	<script charset="utf-8" src="${pageContext.request.contextPath}/kindeditor/lang/zh_CN.js"></script>
	<script charset="utf-8" src="${pageContext.request.contextPath}/kindeditor/plugins/code/prettify.js"></script>
	<script src="${pageContext.request.contextPath}/js/editor.js"></script>
	 <script>
		KindEditor.options.filterMode = false;
		KindEditor.ready(function(K) {
			var editor = K.create('textarea[name="content"]', {
				cssPath : '/web-design/kindeditor/plugins/code/prettify.css',
				uploadJson : '/web-design/upload/image.json',
				fileManagerJson : '/web-design/upload/image.json',
				allowFileManager : true
			});
			prettyPrint();
		});
	</script>
</head>
<body >
	<div style="margin-top: 20px">
		<textarea id="editor_id" name="content" style="width:100%;height:100%;">
			&lt;strong&gt;HTML内容&lt;/strong&gt;
		</textarea>
	</div>
	<input type="button" value="在页面中显示" onclick="show();" style="margin-top: 50px">
	<div id="show" style="margin-top: 50px;">
	aaa
	</div>
	
	
</body>
</html>
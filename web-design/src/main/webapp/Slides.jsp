<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/jsp/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="${pageContext.request.contextPath}/js/slides.jquery.js"></script>

<script src="${pageContext.request.contextPath}/js/common/slides.min.jquery.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/common/Slides.css">

<script>  

$(function(){
	$("#slides").slides({  
		preload: true,  
	    play: 5000,
	    pause: 2500,
	    hoverPause: true ,
	    generateNextPrev: true ,
	    next: '下一页',
	    prev: '上一页'
	  });  
});
</script> 
<title>web-design</title>
</head>
<body>
	<div id="slides">
  <div class="slides_container">
    <div><img src="${pageContext.request.contextPath}/images/39.jpg" style="width: 600px;height: 300px"></div>
    <div><img src="${pageContext.request.contextPath}/images/2eeccf15787aabeca4e580f9d642bbe6.jpg" style="width: 600px;height: 300px"></div>
  	<div><img src="${pageContext.request.contextPath}/images/23deb3240cec653cc89559ac.jpg" style="width: 600px;height: 300px"></div>
  	<div><img src="${pageContext.request.contextPath}/images/163986.jpg" style="width: 600px;height: 300px"></div>
  	<div><img src="${pageContext.request.contextPath}/images/12813299_171949233134_2.jpg" style="width: 600px;height: 300px"></div>
  </div>
</div>
</body>
</html>
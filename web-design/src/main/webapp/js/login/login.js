$(function(){
		$('#password').focus(function(){
			$('#cat').attr('src','images/login/closeeyes.jpg');
		});	
		$('#password').blur(function(){
			$('#cat').attr('src','images/login/openeyes.jpg');
		});
		
		
		$('.regist-style').hide();
		$('.frame-style').show();
		
});

function login(){
	if($('#account').val() == ''){
		alert('账户名称不能为空');
		return ;
	}
	if($('#password').val() == ''){
		alert('密码不能为空');
		return ;
	}
	var obj = {};
	obj.account = $('#account').val();
	obj.password = $('#password').val();
	obj.type = '2';
	$.ajax({
		type : 'POST',
		dataType : 'json',
		data : obj,
		url : '/web-design/user/login.json',
		success :function(result){
			alert(result.respMessage);
		},
		error : function(result){
			alert(result.respMessage);
		}
		
	});
}

function regist(){
	var email_filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if($('#regist_account').val() == ''){
		alert('账户不能为空');
		return ;
	}
	if(!email_filter.test($('#regist_account').val())){
		alert('账户格式不正确');
		return ;
	}
	
	if($('#regist_password').val() == ''  || $('#regist_password2').val() == ''){
		alert('密码不能为空');
		return ;
	}
	if($('#regist_password').val().length <6 || $('#regist_password').val().length >13){
		alert('密码长度不再6-12位之内');
		return ;
	}
	if($('#regist_password').val() != $('#regist_password2').val()){
		alert('两次密码不一致');
		return ;
	}
	if($('#regist_uname').val() == '' ){
		alert('用户名称不能为空');
		return ;
	}
	
	var obj = {};
	obj.account = $('#regist_account').val();
	obj.password = $('#regist_password').val();
	obj.uname = $('#regist_uname').val();
	obj.mobile = $('#regist_mobile').val();
	obj.type = '2';
	obj.email = $('#regist_account').val();
	
	$.ajax({
		type : 'POST',
		dataType : 'json',
		data : obj,
		url : '/web-design/user/regist.json',
		success :function(result){
			alert(result.respMessage);
		},
		error : function(result){
			alert(result.respMessage);
		}
		
	});
}

function showLogin(){
	$('.regist-style').hide();
	$('.frame-style').show();
}

function showRegist(){
	$('.frame-style').hide();
	$('.regist-style').show();
}
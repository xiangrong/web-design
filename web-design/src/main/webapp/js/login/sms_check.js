

function getSmsCode(){
	var myreg = /^0?1[3|5|8][0-9]\d{8}$/; 
	if($('#mobile').val() == ''){
		alert('手机号码不能为空');
		return ;
	}
	if(!myreg.test($('#mobile').val())){
		alert('手机号码不合适');
		return ;
	}
	
	var obj = {};
	obj.mobile = $('#mobile').val();
	$.ajax({
		type: 'POST',
		dataType: 'json',
		data: obj,
		url : '/web-design/sms/getSmsCode.json',
		success :function(result){
			alert(result.respMessage);
		},
		error : function(result){
			alert(result.respMessage);
		}
	});
}



function checkSmsCode(){
	var myreg = /^0?1[3|5|8][0-9]\d{8}$/; 
	if($('#mobile').val() == ''){
		alert('手机号码不能为空');
		return ;
	}
	if(!myreg.test($('#mobile').val())){
		alert('手机号码不合适');
		return ;
	}
	if($('#code').val() == ''){
		alert('验证码不能为空');
		return ;
	}
	
	var obj = {};
	obj.mobile = $('#mobile').val();
	obj.code = $('#code').val();
	$.ajax({
		type: 'POST',
		dataType: 'json',
		data: obj,
		url : '/web-design/sms/checkSmsCode.json',
		success :function(result){
			alert(result.respMessage);
		},
		error : function(result){
			alert(result.respMessage);
		}
	});
}

//全局变量
var RootPath = '';

$(function($) {
	if(RootPath==''){
		//获取当前网址，如： http://localhost:8080/fits/share/meun.jsp
		var curWwwPath = window.document.location.href;
		//获取主机地址之后的目录，如： fits/share/meun.jsp
		var pathName = window.document.location.pathname;
		var pos = curWwwPath.indexOf(pathName);
		//获取主机地址，如： http://localhost:8080
		var localhostPaht = curWwwPath.substring(0, pos);
		//获取带"/"的项目名，如：/fits
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
		
		RootPath = (localhostPaht + projectName);
	} 
});

//滚动窗口高度
function toTop(){
	var toTop = 50;
	if($.browser.msie) {
		toTop = document.documentElement.scrollTop + 50;
	}else{
		toTop = window.pageYOffset + 50;
	}
}

//获取当前世界时间
function currentUTCTime(fmt){
	var date = new Date();
	var year = date.getUTCFullYear();
	var month = date.getUTCMonth()+1;
	var day = date.getUTCDate();
	var hour = date.getUTCHours();
	var minute = date.getUTCMinutes();
	var second = date.getUTCSeconds();
	var result = '';
	if($.trim(fmt).length==0)
		result = 'y-m-d h:i:s';
	else 
		result = fmt.toLocaleLowerCase();
	
	result = result.replace('y',year+'')
	         		.replace('m',formatNum(month))
	         		.replace('d',formatNum(day))
	         		.replace('h',formatNum(hour))
	         		.replace('i',formatNum(minute));
	
	if(result.indexOf('s')>0)
		result = result.replace('s',formatNum(second));
	
	return result;
}

//获取当前时间
function currentTime(fmt){
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	var hour = date.getHours();
	var minute = date.getMinutes();
	var second = date.getSeconds();
	var result = '';
	if($.trim(fmt).length==0)
		result = 'y-m-d h:i:s';
	else 
		result = fmt.toLocaleLowerCase();
	
	result = result.replace('y',year+'')
	         		.replace('m',formatNum(month))
	         		.replace('d',formatNum(day))
	         		.replace('h',formatNum(hour))
	         		.replace('i',formatNum(minute));
	
	if(result.indexOf('s')>0)
		result = result.replace('s',formatNum(second));
	
	return result;
}
//判读日期位是否补零
function formatNum(num){
	if($.trim(num).length<2){
		num = '0' + num;
	}
	return num;
}

function sleeper(numberMillis) { 
	var now = new Date(); 
	var exitTime = now.getTime() + numberMillis; 
	while (true) {
		now = new Date(); 
		if (now.getTime() > exitTime) 
			return; 
	} 
}


/*$(function(){
	submitSelected(dealSelected);
});*/

function submitSelected(callback) {  
    var data = '{id:1,"name":"达达"}';
    callback(data);  
}  
 
function dealSelected(data) { 
	var jsonData = eval('(' + data + ')');
    alert(jsonData.id + ', ' + jsonData.name);  
}  
 


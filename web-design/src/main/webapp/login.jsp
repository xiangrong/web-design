<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/jsp/common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login/login.css">
<script src="${pageContext.request.contextPath}/js/login/login.js"></script>
</head>
<body class="loginbody">

	<img src="${pageContext.request.contextPath}/images/login/loginbackup.jpg">
	<div class="tou-style">
		<a href="#" onclick="showLogin()">登录</a>
		<a href="#" onclick="showRegist()">注册</a>	
	</div>
		
	<div class="regist-style" >
		<span class="regist-title-style">USER REGIST</span>
		<input id="regist_account"  type="text" class="regist-input-style" autocomplete="off" placeholder="USER YOUR EMAIL MAKE ACCOUNT" />
		<input id="regist_password"  type="password" class="regist-input-style" autocomplete="off" placeholder="PASSWORD" />
		<input id="regist_password2"  type="password" class="regist-input-style" autocomplete="off" placeholder="PASSWORD AGAIN" />
		<input id="regist_uname"  type="text" class="regist-input-style" autocomplete="off" placeholder="YOUR NAME" />
		<input id="regist_mobile"  type="text" class="regist-input-style" autocomplete="off" placeholder="YOUR MOBILE" />
		<input id="login" onclick="regist()" href="#" type="button" class="regist-button-style" value="REGIST">
	</div>
	
	<div class="frame-style">
		<img src="${pageContext.request.contextPath}/images/login/openeyes.jpg" id="cat" class="cat-style"/>
		<div style="float:right;height:auto;">
			<span class="login-title-style">WEB-DESIGN  LOGIN</span>
			<div class="frame2-style">
				<img class="image-style" src="${pageContext.request.contextPath}/images/login/user.png"/>
				<input id="account"  class="input-style" autocomplete="off" placeholder="EMAIL/USERNAME/MOBILEPHONE" />
			
			</div>
			<div class="frame2-style">
				<img  class="image-style" src="${pageContext.request.contextPath}/images/login/password.png"/>
				<input id="password" type="password" class="input-style" autocomplete="off" placeholder="PASSWORD" />
			
			</div>
			<div class="frame2-style">
				<input id="login" onclick="login()" href="#" type="button" class="button-style" value="LOGIN TO YOUR ACCOUNT">
			</div>
		</div>
	</div>


</body>
</html>